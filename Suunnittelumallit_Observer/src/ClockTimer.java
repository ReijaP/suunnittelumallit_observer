
import java.util.*;
import java.util.Observable;

public class ClockTimer extends Observable implements Runnable{
	
	private int second = 0, minute = 0, hour = 0;
	private String aika = null;
	
	public ClockTimer() {
	}
	
	public void start() {
	}
	
	public void run() {
		while (true) {
			try {
				Thread.sleep(1000);
				second++;
				if(second % 60 == 0) {
					minute++;
					second = 0;
					if (minute % 60 == 0) {
						hour ++;
						minute = 0;
					}
				}
				aika = hour + ":" + minute + ":" + second;
				setChanged();
				notifyObservers(aika);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}	

}


