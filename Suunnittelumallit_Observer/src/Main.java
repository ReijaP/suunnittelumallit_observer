import java.util.Observable;
import java.util.Observer;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		
		Observable timer = new ClockTimer();
		
		Observer dc = new DigitalClock();
		
		timer.addObserver(dc);
		
		new Thread((Runnable) timer).start();
		

	}

}
